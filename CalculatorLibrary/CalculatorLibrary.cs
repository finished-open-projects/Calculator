﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace CalculatorLibrary
{
    public class Calculator
    {
        readonly JsonWriter writer;
        public Calculator()
        {
            StreamWriter logFile = File.CreateText("calculator.log");
            Trace.Listeners.Add(new TextWriterTraceListener(logFile));
            Trace.AutoFlush = true;
            Trace.WriteLine("Starting Calculator log");
            Trace.WriteLine(string.Format("Started {0}", DateTime.Now.ToString()));

            StreamWriter jsonLog = File.CreateText("calculatorlog.json");
            jsonLog.AutoFlush = true; 
            writer = new JsonTextWriter(jsonLog);
            writer.Formatting = Formatting.Indented;
            writer.WriteStartObject();
            writer.WritePropertyName("Operations");
            writer.WriteStartArray();
        }
        public double DoOperation(double num1, double num2, string op)
        {
            double result = double.NaN;

            writer.WriteStartObject();
            writer.WritePropertyName("Operand 1");
            writer.WriteValue(num1);
            writer.WritePropertyName("Operand 2");
            writer.WriteValue(num2);
            writer.WritePropertyName("Operation");

            switch (op)
            {
                case "a":
                    result = num1 + num2;
                    Trace.WriteLine(string.Format("{0} + {1} = {2}", num1, num2, result));
                    writer.WriteValue("Add");
                    break;
                case "s":
                    result = num1 - num2;
                    Trace.WriteLine(string.Format("{0} - {1} = {2}", num1, num2, result));
                    writer.WriteValue("Subtract");
                    break;
                case "m":
                    result = num1 * num2;
                    Trace.WriteLine(string.Format("{0} * {1} = {2}", num1, num2, result));
                    writer.WriteValue("Multiply");
                    break;
                case "d":
                    while (num2 == 0)
                    {
                        Console.WriteLine("It's not possible to divide by zero. " +
                            "Please, pick a number above:");
                        num2 = DealWithAZeroInput(num2);
                    }
                    result = num1 / num2;
                    Trace.WriteLine(string.Format("{0} / {1} = {2}", num1, num2, result));
                    writer.WriteValue("Divide");
                    break;
                default:
                    break;
            }
            writer.WritePropertyName("Result");
            writer.WriteValue(result);
            writer.WriteEndObject();

            return result;
        }

        public void FinishJson()
        {
            writer.WriteEndArray();
            writer.WriteEndObject();
            writer.Close();
        }

        public static double DealWithAZeroInput(double num2)
        {
            if (!double.TryParse(Console.ReadLine(), out num2))
            {
                Console.WriteLine("Invalid input, type an integer number: ");
                return DealWithAZeroInput(num2);
            }
            else
                return num2;
        }
    }
}